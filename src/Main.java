import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        Integer max = Integer.MIN_VALUE;
        Integer min = Integer.MAX_VALUE;


        BigDecimal a1 = BigDecimal.valueOf(123499, 2);

        MathContext m = new MathContext(4); // 4 precision

        BigDecimal b2 = a1.round(m);
        System.out.println(b2);

        Zmogus zmogus1 = new Zmogus();
        zmogus1.vardas = "aaa";
        zmogus1.pavarde = "yyy";

        Zmogus zmogus2 = new Zmogus();
        zmogus2.vardas = "aaa";
        zmogus2.pavarde = "zzz";

        Zmogus zmogus3 = new Zmogus();
        zmogus3.vardas = "ccc";
        zmogus3.pavarde = "123";



        List<Zmogus> zmones = new ArrayList<>();
        zmones.add(zmogus1);
        zmones.add(zmogus2);
        zmones.add(zmogus3);

        Collections.sort(zmones);


        Map<String, Integer> salys = new TreeMap<>();
        salys.put("ZZ", 41500000);
        salys.put("LT", 2800000);
        salys.put("LV", 2100000);
        salys.put("EE", 1500000);
        salys.put("PL", 41500000);


        salys.put("LT", 500);

        Integer a = salys.get("LT");
        a++;
        salys.put("LT", a);


       System.out.println(salys.hashCode());

        salys.size();
        salys.remove("ZZ");

        salys.put("AA", 100);

        for(int i = 0; i < salys.size(); i++) {

        }


        /* Kaip atspausdinti visus map tipo elementus */
        for (Map.Entry<String, Integer> salis : salys.entrySet()) {
            System.out.println(salis.getKey()  + " " + salis.getValue() );
        }

        salys.forEach((countryCode, population) -> {
            System.out.println("Key : " + countryCode + " Value : " + population);
        });

        Scanner input = new Scanner(System.in);
        String countryCode = "";
        System.out.println("Iveskite salies koda: ");
        countryCode = input.next(); // LT

        System.out.println(salys.get(countryCode));


    }
}
