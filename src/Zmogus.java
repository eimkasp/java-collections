public class Zmogus implements Comparable<Zmogus>{

    public String vardas;
    public String pavarde;


    @Override
    public int compareTo(Zmogus o) {
        if(this.vardas.equals(o.vardas)) {
            return this.pavarde.compareTo(o.pavarde);
        } else {
            return this.vardas.compareTo(o.vardas);
        }
    }
}
